
<!-- Title Recommendation: EVENT: Event title - (Event Dates) -->

## Event Details

- Event Title:
- Event Dates: 
- Event Type:  <!-- GitLab-Sponsored, GitLab-Event, Community-Event, External -->
- Budget:

## Thought Leadership Impact

## Staffing

<!-- Organizer, Booth Duty, Speaker, Participant -->

| Team Member | Role    |
|-------------|---------|
|             |         |

### CFP Details

- Link to CFP Issue:
- Number of Team Acceptance:

## Event KPIs

<!-- What does success for this event look like -->

- 
- 


### Call To Actions

-
-

### Campaign / UTM Tracking

Use the Community Relations UTM Strategy to generate UTM tracking codes


## Checklist

- [ ] Add to [Team Calendar](https://handbook.gitlab.com/handbook/marketing/developer-relations/developer-advocacy/#-team-calendar)
- [ ] Confirm Staffing
- [ ] Define KPIs & CTAs
- [ ] Generate and Validate UTM & Campaign codes
- [ ] Updated Issue with KPI result.


## Event Report & Results Generated

Data showing results generated during the event.

<!-- Keep the labels intanct. -->

/label ~"DA-Type::Events" ~"DA-Status::ToDo" ~"DevRel-Events" ~"developer-advocacy" 

/assign @johncoghlan @dnsmichi

/cc @gitlab-da

<!-- Please add a due date (for event speaking requests, content delivery, etc.) - e.g. /due 2025-07-31 -->
/due 

<!-- FY26 events strategy epic -->
/epic https://gitlab.com/groups/gitlab-com/marketing/developer-relations/-/epics/513 
