
We host a monthly LinkedIn Live broadcast to highlight our monthly release, and share product updates and thought leadership. 
[Handbook link for more info on LinkedIn Lives and panelist preparation info](https://handbook.gitlab.com/handbook/marketing/developer-relations/developer-advocacy/projects/#linkedin-lives-in-collaboration-with-the-social-team)

## Overview

- **Type**: 
- **Host**: 
- **Panelists**: 
- **Date**: 
- **Release**: 

## Segment Ideas: 

1. Idea 1
2. Idea 2
3. Idea 3

## Resources
- [Organic Social Media Planning Issue of the Month](#)

## Action Items:

- [ ] Determine whether we're going to do live or pre-recorded. 
- [ ] Finalize topics alongside the release. 
- [ ] Reach out to panelists. 
- [ ] Start the script doc [sample doc](#)
- [ ] Schedule a rehearsal call.
- [ ] Follow up with metrics. 

/cc @sugaroverflow @tschraeder-rodriguez @omora1 @jrubenoff 

/confidential

/label ~"DA-Type::Content" ~"DA-Type-Content::video" ~"FY26-Q1" ~"DA-Status::ToDo" 

<!-- Please add a due date (for event speaking requests, content delivery, etc.) - e.g. /due 2025-07-31 -->
/due 