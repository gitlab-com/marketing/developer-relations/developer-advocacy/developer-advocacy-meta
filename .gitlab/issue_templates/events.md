> Issue Title Guide:
> Dates should be using ISO dates, see https://about.gitlab.com/handbook/communication/#writing-style-guidelines
> Format:

>  Event: Event Name, Location, ISO Date

## Activity Summary


Provide a brief summary of the activity, including:

- Type of activity (e.g., conference, meetup, webinar)
- Date and location:
- Key topics or themes to be covered:
    - Topic 1
    - Topic 2
    - Topic 3



## Learnings

Capture and document key learnings from the event, including:

- Insights gained from attendees, competitors, or industry trends
- Feedback on GitLab's products or presentations
- Potential partnership or collaboration opportunities
- Areas for improvement in future events


## Action Items

Based on the learnings, list specific action items to follow up on:

- [ ] [Create event report slides and share them](/handbook/marketing/developer-relations/events/#event-reporting)
- [ ] Share key insights with relevant internal teams (e.g., Product, Sales, Marketing)
- [ ] Follow up with potential leads or partners identified during the event
- [ ] Update relevant documentation or marketing materials based on feedback received
- [ ] Schedule a debrief meeting with the team to discuss learnings and next steps
- [ ] Identify opportunities for future speaking engagements or sponsorships
- [ ] Update our competitive intelligence based on insights gained from the event


## Relevant Issues, Epics or resources

-


<!-- these labels should be included on all templates -->
/label ~"developer-advocacy"

<!-- Example: These are samples for guidance, please add relevant labels for activity region, type, quarter or any other labels relevant to your team/program. Please include additional relevant labels here.  -->

/label ~"DA-Type::Events" ~"Region-AMER" ~"FY26-Q1" ~"DA-Status::ToDo"

/due in 1 month

<!-- Events epic (FY26) -->
/epic https://gitlab.com/groups/gitlab-com/marketing/developer-relations/-/epics/513 



<!-- Mention team members that should be aware of the epic -->
/cc